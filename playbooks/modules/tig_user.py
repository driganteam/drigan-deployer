#!/usr/bin/python

from ansible.module_utils.basic import *

def main():
    module = AnsibleModule(
        argument_spec=dict(
            login_user=dict(default="postgres"),
            login_password=dict(default=""),
            login_host=dict(default=""),
            login_unix_socket=dict(default=""),
            port=dict(default="5432"),
            username=dict(default=None),
            password=dict(default=None),
            database=dict(),
            state=dict(default="present", choices=["absent", "present"]),
        ),
        supports_check_mode = True
    )

    try:
      import psycopg2
      import psycopg2.extras
    except:
        module.fail_json(msg="the python psycopg2 module is required")

    database=module.params['database']
    state = module.params["state"]
    username = module.params['username']
    password = module.params['password']

    # To use defaults values, keyword arguments must be absent, so
    # check which values are empty and don't include in the **kw
    # dictionary
    params_map = {
        "login_host":"host",
        "login_user":"user",
        "login_password":"password",
        "port":"port"
    }
    kw = dict( (params_map[k], v) for (k, v) in module.params.iteritems()
              if k in params_map and v != '' )

    # If a login_unix_socket is specified, incorporate it here.
    is_localhost = "host" not in kw or kw["host"] == "" or kw["host"] == "localhost"
    if is_localhost and module.params["login_unix_socket"] != "":
        kw["host"] = module.params["login_unix_socket"]

    # module.fail_json(**kw)

    try:
        db_connection = psycopg2.connect(database=database, **kw)
        # Enable autocommit so we can create databases
        if psycopg2.__version__ >= '2.4.2':
            db_connection.autocommit = True
        else:
            db_connection.set_isolation_level(psycopg2
                                              .extensions
                                              .ISOLATION_LEVEL_AUTOCOMMIT)
        cursor = db_connection.cursor(
                cursor_factory=psycopg2.extras.DictCursor)

        cursor.execute(
            "SELECT user_id, user_pw FROM tig_users WHERE user_id=%s",
            (username, ))
        result = cursor.fetchone()

        if module.check_mode:
          if result is None:
            module.exit_json(changed=state=="absent")
          username, pw = result
          if pw != password:
            module.exit_json(changed=True)
          module.exit_json(changed=state=="present")

        if result is None:
          if state=="present":
            cursor.execute("SELECT tigadduserplainpw(%s, %s);", (username, password))
            module.exit_json(changed=True, mag='Created')
          module.exit_json(changed=False)
        username, pw = result
        if state=="absent":
          cursor.execute("SELECT tigremoveuser(%s)", (username, ))
          module.exit_json(changed=True, mag='Deleted')
        if pw == password:
          module.exit_json(changed=False)
        cursor.execute("SELECT tigupdatepasswordplainpw(%s, %s)", (username, password))
        module.exit_json(changed=True, mag='Updated pw')

    except Exception as e:
        module.fail_json(msg="unable to connect to database: %s" % e)

if __name__ == '__main__':
    main()
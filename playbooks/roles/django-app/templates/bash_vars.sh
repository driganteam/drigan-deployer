#!/usr/bin/env bash

{% for name, value in django_envvars.items() %}
export "{{ name.strip() }}"="{{ value.strip() }}"
{% endfor %}
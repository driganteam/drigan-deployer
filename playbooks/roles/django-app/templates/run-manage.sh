#!/usr/bin/env bash

{% for name, value in django_envvars.items() %}
export "{{ name.strip() }}"="{{ value.strip() }}"
{% endfor %}

cd "/home/{{django_app_user}}/drigan"

source "/home/{{ django_app_user }}/venv/bin/activate"

./manage.py $@